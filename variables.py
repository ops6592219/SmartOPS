
node_export = {
    "下载资源文件版本v1.7.0": "wget https://github.com/prometheus/node_exporter/releases/download/v1.7.0/node_exporter-1.7.0.linux-amd64.tar.gz",
    "解压相关文件": "(tar -xvzf node_exporter-1.7.0.linux-amd64.tar.gz  -C /usr/local) && (mv /usr/local/node_exporter-1.7.0.linux-amd64 /usr/local/node_exporter)",
    "清理非必要文件": "(rm -rf node_exporter-1.7.0.linux-amd64.tar.gz) ",
    "注册systemctl 服务": """cat > /usr/lib/systemd/system/node_exporter.service << EOF
                            [Unit]
                            Description=node_exporter
                            Documentation=https://prometheus.io/
                            After=network.target
                            [Service]
                            ExecStart=/usr/local/node_exporter/node_exporter  --web.listen-address=:9100
                            Restart=on-failure
                            [Install]
                            WantedBy=multi-user.target
                            EOF
                        """,
    "启动node-export服务": "(systemctl daemon-reload) &&  (systemctl enable node_exporter.service) && (systemctl start node_exporter.service)",

}

snmp_export = {
    "下载资源文件版本v0.25.0": "wget  https://github.com/prometheus/snmp_exporter/releases/download/v0.25.0/snmp_exporter-0.25.0.linux-amd64.tar.gz",
    "解压相关文件": "(tar -zxvf /root/snmp_exporter-0.25.0.linux-amd64.tar.gz -C /usr/local )&& (mv /usr/local/snmp_exporter-0.25.0.linux-amd64 /usr/local/snmp_exporter)",
    "清理非必要文件": "(rm -rf snmp_exporter-0.25.0.linux-amd64.tar.gz ) ",
    "注册systemctl 服务": """cat > /usr/lib/systemd/system/snmp_exporter.service << EOF
                                [Unit]
                                Description=snmp_exporter
                                After=network.target 

                                [Service]
                                Restart=on-failure
                                ExecStart=/usr/local/snmp_exporter/snmp_exporter --config.file=/usr/local/snmp_exporter/snmp.yml --web.listen-address=:9116 --snmp.wrap-large-counters --log.level=info

                                [Install]
                                WantedBy=multi-user.target
                                EOF
                        """,
    "启动snmp_exporter服务": "(systemctl daemon-reload) &&  (systemctl enable snmp_exporter.service) && (systemctl start snmp_exporter.service)",

}

prometheus = {
    "下载资源文件版本v2.51.2": "wget https://github.com/prometheus/prometheus/releases/download/v2.51.2/prometheus-2.51.2.linux-amd64.tar.gz",
    "解压相关文件": "(tar -zxvf prometheus-2.51.2.linux-amd64.tar.gz -C /etc/) && (mv /etc/prometheus-2.51.2.linux-amd64/ /etc/prometheus)",
    "清理非必要文件": "(rm -rf prometheus-2.51.2.linux-amd64.tar.gz ) ",
    "注册systemctl 服务": """cat > /usr/lib/systemd/system/prometheus.service << EOF
                            [Unit]
                            Description=Prometheus Monitoring System
                            Documentation=Prometheus Monitoring System

                            [Service]
                            ExecStart=/etc/prometheus/prometheus \
                               --config.file=/etc/prometheus/prometheus.yml \
                               --web.enable-remote-write-receiver \
                               --web.enable-lifecycle --web.listen-address=:9090
                            Restart=on-failure
                            [Install]
                            WantedBy=multi-user.target
                            EOF
                        """,
    "启动prometheus服务": "(systemctl daemon-reload) && (systemctl enable prometheus.service) && (systemctl start prometheus.service)",
}

alertmanager = {
    "下载资源文件版本v0.27.0": "wget https://github.com/prometheus/alertmanager/releases/download/v0.27.0/alertmanager-0.27.0.linux-amd64.tar.gz",
    "解压相关文件": "(tar -xvf alertmanager-0.27.0.linux-amd64.tar.gz -C ./ ) && (mv ./alertmanager-0.27.0.linux-amd64/* /etc/prometheus/) ",
    "清理非必要文件": "(rm -rf alertmanager-0.27.0.linux-amd64.tar.gz )&& (rm -rf ./alertmanager-0.27.0.linux-amd64)",
    "注册systemctl 服务": """cat > /usr/lib/systemd/system/alertmanager.service << EOF
                            Description=alertmanager
                            [Service]
                            ExecStart=/etc/prometheus/alertmanager \
                                      --config.file=/etc/prometheus/alertmanager.yml \
                                      --storage.path=/var/lib/prometheus/alertmanager 
                            ExecReload=/bin/kill -HUP $MAINPID
                            KillMode=process
                            Restart=on-failure
                            [Install]
                            WantedBy=multi-user.target
                            EOF
                        """,
    "启动alertmanager服务": "(systemctl daemon-reload)  && (systemctl enable alertmanager.service) && (systemctl start alertmanager.service)",
}
blackbox_export = {
    "下载资源文件版本0.25.0": "wget https://github.com/prometheus/blackbox_exporter/releases/download/v0.25.0/blackbox_exporter-0.25.0.linux-amd64.tar.gz",
    "解压相关文件": "(tar -zxvf blackbox_exporter-0.25.0.linux-amd64.tar.gz -C /usr/local  ) && (mv /usr/local/blackbox_exporter-0.25.0.linux-amd64 /usr/local/blackbox_exporter)",
    "清理非必要文件": "(rm -rf blackbox_exporter-0.25.0.linux-amd64.tar.gz )",
    "备份配置文件": "( cp  /usr/local/blackbox_exporter/blackbox.yml  /usr/local/blackbox_exporter/blackbox.yml-bak)",
    "修改配置文件": """cat > /usr/local/blackbox_exporter/blackbox.yml << EOF
                     modules:
                       http_2xx:
                         prober: http
                         timeout: 15s
                         http:
                           fail_if_not_ssl: true
                           ip_protocol_fallback: false
                           method: GET
                           follow_redirects: true
                           preferred_ip_protocol: ip4
                           valid_http_versions:
                             - HTTP/1.1
                             - HTTP/2.0
                           valid_status_codes: [200,301,302,304]
                       http_post_2xx:
                         prober: http
                         http:
                           method: POST
                    EOF
                """,
    "注册systemctl 服务": """cat > /usr/lib/systemd/system/blackbox_exporter.service << EOF
                            [Unit]
                            Description=blackbox_exporter
                            After=network.target
                            [Service]
                            ExecStart=/usr/local/blackbox_exporter/blackbox_exporter --config.file=/usr/local/blackbox_exporter/blackbox.yml
                            Restart=on-failure
                            [Install]
                            WantedBy=multi-user.target
                            EOF
                    """,
    "启动blackbox_exporter服务": "(systemctl daemon-reload)  && (systemctl enable blackbox_exporter) && (systemctl start blackbox_exporter)",

}
