import os, yaml, json, click, subprocess
from variables import *

COMMAND_LIST = {"node-export": node_export, "prometheus": prometheus, "alertmanager": alertmanager,
                "snmp-export": snmp_export, "blackbox-export": blackbox_export}


def version(ctx, param, value):
    if not value or ctx.resilient_parsing:
        return
    click.echo('Tools Version 1.0.2')
    ctx.exit()


#  install 相关
def run_bash(command):
    if not subprocess.run(command, shell=True).returncode:
        pass
    else:
        click.echo(f"执行{command}命令错误")


def install_list(ctx, param, value):
    if not value or ctx.resilient_parsing:
        return
    click.echo("软件列表:")
    for name in COMMAND_LIST.keys():
        click.echo(f"{name}")
    ctx.exit()


# yaml 转 json 相关
def translation_json(yamlpath):
    write_datas = []
    target_list = []

    with open(yamlpath, encoding="utf-8") as f:
        dict_datas = yaml.load(f, Loader=yaml.FullLoader)
        for target, labels in dict_datas.items():
            target_datas = {}
            if labels:
                target_list.append(target)
                target_datas["targets"] = target_list
                target_datas["labels"] = labels
                write_datas.append(target_datas)
                target_list = []
            else:
                target_list.append(target)

        json_data = json.dumps(write_datas, ensure_ascii=False, separators=(",", ':'))

    # 回显输出
    click.echo("\n" + "-*-" * 50)
    click.echo(f"转换后字符为: {json_data}")
    click.echo("-*-" * 50 + "\n")

    newfile = yamlpath.rsplit(".", 1)[0] + ".json"
    with open(newfile, "w") as f:
        f.write(json_data)
        print(f"文件输出:{newfile}")


def get_file_path(path, filename):
    # 判断文件为绝对路径
    if filename.startswith("/"):
        filepath = filename
    else:
        # 判断相对路径
        filename = "/" + filename if "/" in filename else "/" + filename
        filepath = path + filename
    return filepath


### TODO: 添加--descripe 描述版本更新内容
@click.group()
@click.option('-v', '--version', is_flag=True, callback=version, expose_value=False, is_eager=True)
def cli():
    """ 这是一个系统工具"""
    pass


### TODO : 添加指定install软件版本 功能
@cli.command()
@click.option("-s", "--softwre", help="安装软件名")
@click.option("-ls", "--list", is_flag=True, callback=install_list, help="查看支持的软件名")
def install(softwre, list):
    """ 安装常用工具"""
    try:
        if softwre in COMMAND_LIST.keys():
            click.echo(f"准备安装工具:{softwre}")
            for message, command in COMMAND_LIST[softwre].items():
                click.echo(f"当前执行:{message}")
                run_bash(command)
        else:
            click.echo(f"请检查包名{softwre}")
    except click.exceptions.ClickException as e:
        click.echo(f"错误: {str(e)}")


@cli.command()
@click.option("-f", "--filename", help="input a file type .yaml")
def tranJson(filename=None):
    """
    将yaml 转换为 json
    """
    if filename.split(".")[-1] == "yaml":
        filepath = get_file_path(os.path.abspath(os.curdir), filename)
        translation_json(filepath)
    else:
        print("文件有误请重新输入")


if __name__ == '__main__':
    cli()
