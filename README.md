
# SmartOPS



## 本地化项目启动
```
# 配置加载环境
  pip3 install virtualenv
  virtualenv venv  
  source venv/bin/activate
  pip3 install -r requirements.txt 

# 运行工具
  python3 tools.py 

```

## 编译为可执行程序
```commandline
pip3 install pyinstaller

 pyinstaller --onefile tools.py 
 
 注意: 不同系统需要单独编译
 
 查看文件支持的系统 file
 例:
    (venv)Dong SmartOPS % file dist/tools 
          dist/tools: Mach-O 64-bit executable arm64
```
## 目的
- 简化运维操作

## 功能介绍
```commandline
 python3 tools.py --help  查看帮助信息

Commands:

tranjson 
 - tranjson -f filename.yaml  yaml转json 用于prometheus 动态文件生成
 
install 
 - install -ls 查看支持的软件列表
 - install -s [软件名] 安装指定软件
```
## 问题

### zsh: command not found: virtualenv
处理:
```commandline
pip3 install virtualenv

export PATH="$HOME/Library/Python/3.9/bin:$PATH"
```